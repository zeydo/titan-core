<?php

if (!function_exists('app')) {
    /**
     * Get application container or a service
     *
     * @param string|null $name
     * @return mixed
     */
    function app($name = null)
    {
        if (is_null($name))
            return \Titan\Kernel\Facade::getFacadeApplication();

        return app()->resolve($name);
    }
}

if (!function_exists('config')) {
    /**
     * Get config service or an item
     *
     * @param string|null $item
     * @return mixed
     */
    function config($item = null)
    {
        if (is_null($item))
            return app('config');

        $keys = explode('.', $item);

        $file = $keys[0];

        array_shift($keys);

        return app('config')->load($file)->get(implode('.', $keys));
    }
}

if (!function_exists('root_path')) {
    /**
     * Return root directory
     *
     * @param string|null $path
     * @return string
     */
    function root_path($path = null)
    {
        $rootPath = app()->get('root_path');

        return is_null($path) ? $rootPath : $rootPath . DS . $path;
    }
}

if (!function_exists('base_path')) {
    /**
     * Return base directory
     *
     * @param string|null $path
     * @return string
     */
    function base_path($path = null)
    {
        $basePath = app()->get('base_path');

        return is_null($path) ? $basePath : $basePath . DS . $path;
    }
}

if (!function_exists('app_path')) {
    /**
     * Return app directory
     *
     * @param string|null $path
     * @return string
     */
    function app_path($path = null)
    {
        $appPath = app()->get('app_path');

        return is_null($path) ? $appPath : $appPath . DS . $path;
    }
}

if (!function_exists('public_path')) {
    /**
     * Return public directory
     *
     * @param string|null $path
     * @return string
     */
    function public_path($path = null)
    {
        $publicPath = app()->get('public_path');

        return is_null($path) ? $publicPath : $publicPath . DS . $path;
    }
}

if (!function_exists('model_path')) {
    /**
     * Return models directory
     *
     * @param string|null $path
     * @return string
     */
    function model_path($path = null)
    {
        $modelPath = app()->get('model_path');

        return is_null($path) ? $modelPath : $modelPath . DS . $path;
    }
}

if (!function_exists('view_path')) {
    /**
     * Return views directory
     *
     * @param string|null $path
     * @return string
     */
    function view_path($path = null)
    {
        $viewPath = app()->get('view_path');

        return is_null($path) ? $viewPath : $viewPath . DS . $path;
    }
}

if (!function_exists('config_path')) {
    /**
     * Return config directory
     *
     * @param string|null $path
     * @return string
     */
    function config_path($path = null)
    {
        $configPath = app()->get('config_path');

        return is_null($path) ? $configPath : $configPath . DS . $path;
    }
}

if (!function_exists('storage_path')) {
    /**
     * Return storage directory
     *
     * @param string|null $path
     * @return string
     */
    function storage_path($path = null)
    {
        $storagePath = app()->get('storage_path');

        return is_null($path) ? $storagePath : $storagePath . DS . $path;
    }
}