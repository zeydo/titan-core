<?php
/**
 * Titan Mini Framework
 * --
 * Simple and Modern Web Application Framework
 *
 * @author  Turan Karatuğ - <tkaratug@hotmail.com.tr>
 * @web     <http://www.titanphp.com>
 * @docs    <http://docs.titanphp.com>
 * @github  <http://github.com/tkaratug/titanframework>
 * @license The MIT License (MIT) - <http://opensource.org/licenses/MIT>
 */

use Titan\Kernel\Kernel;

/*
|-----------------------------------------------------------------------
| Titan Framework Version Number
|-----------------------------------------------------------------------
*/
define('VERSION', '3.0.0');

/*
|-----------------------------------------------------------------------
| Application Environment
|-----------------------------------------------------------------------
|
| Defining the application environment. ['dev', 'test', 'prod']
|
*/
define('APP_ENV', 'dev');

/*
|-----------------------------------------------------------------------
| Directory Separator
|-----------------------------------------------------------------------
|
| Defining directory separator
|
 */
define('DS', DIRECTORY_SEPARATOR);

/*
|-----------------------------------------------------------------------
| Prepare Titan Kernel
|-----------------------------------------------------------------------
|
| Building structure and registering service providers.
|
*/
$app = new Kernel();

/*
|-----------------------------------------------------------------------
| Run Titan
|-----------------------------------------------------------------------
|
| Awesome!
|
*/
$app->run(APP_ENV);