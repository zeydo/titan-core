<?php

namespace Titan\Exception;

use Exception;

class ExceptionHandler extends Exception
{
    public function __construct($title, $message)
    {
        if (APP_ENV === 'dev') {
            throw new Exception(strip_tags($title . ' - ' . $message), 1);
        }

        return error($title, $message);
    }
}