<?php

namespace Titan\Container;

class Container
{
    /**
     * @var array
     */
    protected $bindings = [];

    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @var Container
     */
    protected static $instance;

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}

    /**
     * Container instance
     *
     * @return Container
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    /**
     * @param string $key
     * @param string $value
     * @param bool $singleton
     */
    public function bind($key, $value, $singleton = false)
    {
        $this->bindings[$key] = [
            'value'     => $value,
            'singleton' => $singleton
        ];
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function singleton($key, $value)
    {
        return $this->bind($key, $value, true);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key) : bool
    {
        return array_key_exists($key, $this->bindings);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->bindings[$key]['value'];
    }

    /**
     * @param $key
     * @param array $args
     * @return mixed|null
     * @throws \ReflectionException
     */
    public function resolve($key, array $args = [])
    {
        $class = $this->getBinding($key);

        if ($class === null) {
            $class = [
                'value' => $key,
                'singleton' => false
            ];
        }

        if ($this->isSingleton($key) && $this->isSingletonResolved($key))
            return $this->getSingletonInstance($key);

        $object = $this->buildObject($class, $args);

        return $this->prepareObject($key, $object);
    }

    /**
     * @param $class
     * @param array $args
     * @return object
     * @throws \ReflectionException
     */
    protected function buildObject($class, array $args = [])
    {
        $className = $class['value'];

        $reflector = new \ReflectionClass($className);

        if (!$reflector->isInstantiable())
            throw new \Exception("Class [$className] is not a resolvable dependency!");

        if ($reflector->getConstructor() !== null) {
            $constructor    = $reflector->getConstructor();
            $params         = $constructor->getParameters();
            $args           = $this->buildDependencies($args, $params);

            return $reflector->newInstanceArgs($args);
        }

        return $reflector->newInstance();
    }

    /**
     * @param $args
     * @param $params
     * @return mixed
     * @throws \ReflectionException
     */
    protected function buildDependencies($args, $params)
    {
        foreach ($params as $param) {
            if ($param->isOptional()) continue;
            if ($param->isArray()) continue;

            $class = $param->getClass();

            if ($class === null) continue;

            if (get_class($this) === $class->name) {
                array_push($args, $this);
                continue;
            }

            array_push($args, $this->resolve($class->name));
        }

        return $args;
    }

    /**
     * @param string $key
     * @param $object
     * @return mixed
     */
    protected function prepareObject($key, $object)
    {
        if ($this->isSingleton($key))
            $this->instances[$key] = $object;

        return $object;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    protected function getBinding($key)
    {
        if (!array_key_exists($key, $this->bindings))
            return null;

        return $this->bindings[$key];
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function isSingleton($key)
    {
        $binding = $this->getBinding($key);

        if ($binding === null)
            return false;

        return $binding['singleton'];
    }

    /**
     * @param string $key
     * @return bool
     */
    protected function isSingletonResolved($key)
    {
        return array_key_exists($key, $this->instances);
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    protected function getSingletonInstance($key)
    {
        return $this->isSingletonResolved($key) ? $this->instances[$key] : null;
    }

    /**
     * @param $key
     * @return mixed|null
     * @throws \ReflectionException
     */
    public function offsetGet($key)
    {
        return $this->resolve($key);
    }

    /**
     * @param $key
     * @param $value
     */
    public function offsetSet($key, $value)
    {
        return $this->bind($key, $value);
    }

    /**
     * @param $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->bindings);
    }

    /**
     * @param $key
     */
    public function offsetUnset($key)
    {
        unset($this->bindings[$key]);
    }
}