<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MiddlewareCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:middleware')
            ->addArgument('name', InputArgument::REQUIRED, 'The name for the middleware.')
            ->addOption('--force', '-f', InputOption::VALUE_OPTIONAL, 'Force to re-create middleware.')
            ->setDescription('Create a new middleware.')
            ->setHelp("This command makes you to create middleware...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $force = $input->hasParameterOption('--force');
        $file = ROOT . '/App/Middlewares/' . $name . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($file, $name);
            $output->writeln(
                "\n" . ' <info>+Success!</info> "' . ($name) . '" middleware created.'
            );
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($file, $name);
                $output->writeln(
                    "\n" . ' <info>+Success!</info> "' . ($name) . '" middleware re-created.'
                );
            } else {
                $output->writeln(
                    "\n" . ' <error>-Error!</error> Middleware already exists! (' . $name . ')'
                );
            }
        }

        return;
    }

    /**
     * Create new middleware file
     *
     * @param string $file
     * @param string $name
     * @return void
     */
    private function createNewFile($file, $name)
    {
        $middleware = ucfirst($name);
        $contents = <<<PHP
<?php
namespace App\Middlewares;

class $middleware
{
    /**
     * Run before the action
     */
    public function handle()
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }

        return;
    }

}