<?php

namespace Titan\Controller;

use Titan\Libraries\Uri\UriGeneratorInterface;

trait ControllerTrait
{
    /**
     * Get container service
     *
     * @param string $key
     * @return mixed
     */
    protected function get(string $key)
    {
        return $this->container->resolve($key);
    }

    /**
     * Returns true if the service is defined
     *
     * @param string $key
     * @return bool
     */
    protected function has(string $key) : bool
    {
        return $this->container->has($key);
    }

    /**
     * Generates a URL from the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function getUrl(string $route, array $params = [])
    {
        return $this->container->resolve('router')->getUrl($route, $params, UriGeneratorInterface::FULL_URL);
    }

    /**
     * Generates a path from the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function getPath(string $route, array $params = [])
    {
        return $this->container->resolve('router')->getUrl($route, $params, UriGeneratorInterface::FULL_PATH);
    }

    /**
     * Redirect to the given url
     *
     * @param string $url
     * @param int $delay
     * @return mixed
     */
    protected function redirect(string $url, int $delay = 0)
    {
        return $this->container->resolve('uri')->redirect($url, $delay);
    }

    /**
     * Redirects to the route with the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function redirectToRoute(string $route, array $params = [])
    {
        return $this->container->resolve('uri')->redirect($this->getUrl($route, $params));
    }

    /**
     * Returns a json response
     *
     * @param $data
     * @param int $status
     * @return mixed
     */
    protected function json($data, int $status = 200)
    {
        return $this->container->resolve('response')->setStatusCode($status)->json($data);
    }

    /**
     * Set a flash message to the current session
     *
     * @param string $message
     * @return mixed
     */
    protected function setFlash(string $message)
    {
        return $this->container->resolve('session')->setFlash($message);
    }

    /**
     * Get flash message from current session
     *
     * @return mixed
     */
    protected function getFlash()
    {
        return $this->container->resolve('session')->getFlash();
    }

    /**
     * Returns a rendered view
     *
     * @param string $view
     * @param array $params
     * @param bool $cache
     * @return mixed
     */
    protected function render(string $view, array $params = [], $cache = true)
    {
        return $this->container->resolve('view')->render($view, $params, $cache);
    }

    /**
     * Run middlewares from the given parameters
     *
     * @param array $middleware
     * @param bool $default
     */
    protected function middleware(array $middleware, $default = false)
    {
        if ($default) {
            foreach ($middleware as $key => $val) {
                $this->container->resolve($key)->handle();
            }
        } else {
            foreach ($middleware as $class) {
                $this->container->resolve($class)->handle();
            }
        }
    }

    /**
     * Returns a DB instance
     *
     * @return mixed
     */
    protected function db()
    {
        return $this->container->resolve('db');
    }
}