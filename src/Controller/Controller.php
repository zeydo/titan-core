<?php

namespace Titan\Controller;

use Titan\Container\Container;

class Controller
{
    use ControllerTrait;

    /**
     * Application container
     *
     * @var Container
     */
    protected $container;

    /**
     * Controller constructor.
     *
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->container = Container::getInstance();
        $this->middleware($this->container->resolve('config')->load('services')->get('middleware')['default'], true);
    }
}