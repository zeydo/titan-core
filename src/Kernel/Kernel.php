<?php

namespace Titan\Kernel;

use Titan\Container\Container;
use Tracy\Debugger;

class Kernel
{
    /**
     * Application container
     *
     * @var Container
     */
    private $container;

    /**
     * Root directory
     *
     * @var string
     */
    private $rootPath;

    /**
     * Base directory
     *
     * @var string
     */
    private $basePath;

    /**
     * App directory
     *
     * @var string
     */
    private $appPath;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        $this->container    = Container::getInstance();

        $this->rootPath     = realpath(getcwd());
        $this->basePath     = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1));
        $this->appPath      = $this->rootPath . DS . 'App';

        $this->register();
    }

    /**
     * Register paths, service providers and facades
     *
     * @throws \ReflectionException
     */
    protected function register()
    {
        $this->registerPaths();
        $this->registerPrimaryServiceProviders();
        $this->registerPrimaryFacades();
        $this->registerServiceProviders($this->container->resolve('config')->load('services')->get('providers'));
        $this->registerMiddleware($this->container->resolve('config')->load('services')->get('middleware'));
        $this->registerServices($this->container->resolve('config')->load('services')->get('services'));
        $this->resolveFacades($this->container->resolve('config')->load('services')->get('facades'));
    }

    /**
     * Kernel run
     *
     * @param string $env
     * @throws \ReflectionException
     * @return void
     */
    public function run(string $env = 'dev')
    {
        $this->setEnvironment($env);
        $this->setLanguage();

        $this->container->resolve('load')->file(realpath($this->rootPath . '/App/Routes.php'));
        $this->container->resolve('router')->run();
    }

    /**
     * Set environment
     *
     * @param string $env
     * @return void
     */
    protected function setEnvironment(string $env)
    {
        switch ($env) {
            case 'dev':
                ini_set('display_errors', 1);
                error_reporting(1);
                Debugger::enable();
                break;
            case 'test':
            case 'prod':
                ini_set('display_errors', 0);
                error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
                break;
            default:
                header('HTTP/1.1 503 Service Unavailable.', true, 503);
                die('The application environment is not set correctly.');
        }
    }

    /**
     * Set default language
     *
     * @throws \ReflectionException
     */
    protected function setLanguage()
    {
        $config     = $this->container->resolve('config')->load('app');
        $session    = $this->container->resolve('session');

        if (!$session->has('lang')) {
            $session->set('lang', $config->get('languages')[$config->get('locale')]);
        }
    }

    /**
     * Register primary providers to the container
     *
     * @return void
     */
    protected function registerPrimaryServiceProviders()
    {
        (new \Titan\Libraries\Load\LoadServiceProvider($this->container))->register();
        (new \Titan\Libraries\Config\ConfigServiceProvider($this->container))->register();
    }

    /**
     * Register primary facades to the container
     *
     * @return void
     */
    protected function registerPrimaryFacades()
    {
        class_alias(\Titan\Facades\Load::class, 'Load');
        class_alias(\Titan\Facades\Config::class, 'Config');
    }

    /**
     * Register service providers of Application
     *
     * @param array $providers
     * @return void
     */
    protected function registerServiceProviders(array $providers)
    {
        foreach ($providers as $provider) {
            (new $provider($this->container))->register();
        }
    }

    /**
     * Register middleware
     *
     * @param array $middleware
     * @return void
     */
    protected function registerMiddleware(array $middleware)
    {
        foreach ($middleware as $loadKey => $loadVal) {
            foreach ($loadVal as $key => $val) {
                $this->container->bind($key, $val);
            }
        }
    }

    /**
     * Register services
     *
     * @param array $services
     * @return void
     */
    protected function registerServices(array $services)
    {
        foreach ($services as $key => $value) {
            $this->container->bind($key, $value);
        }
    }

    /**
     * Resolve facades
     *
     * @param array $facades
     * @return void
     */
    protected function resolveFacades(array $facades)
    {
        Facade::setFacadeApplication($this->container);

        foreach ($facades as $key => $val) {
            if (!class_exists($key)) {
                class_alias($val, $key);
            }
        }
    }

    /**
     * Register paths to the container
     */
    protected function registerPaths()
    {
        $this->container->bind('root_path', $this->rootPath);
        $this->container->bind('base_path', $this->basePath);
        $this->container->bind('app_path', $this->rootPath . DS . 'App');
        $this->container->bind('model_path', $this->appPath . DS . 'Models');
        $this->container->bind('view_path', $this->appPath . DS . 'Views');
        $this->container->bind('config_path', $this->appPath . DS . 'Config');
        $this->container->bind('public_path', $this->rootPath . DS . 'Public');
        $this->container->bind('storage_path', $this->rootPath . DS . 'Storage');
    }
}