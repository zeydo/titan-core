<?php

namespace Titan\Kernel;

use Titan\Container\Container;

abstract class ServiceProvider
{
    /**
     * Application container
     *
     * @var Container $app
     */
    protected $app;

    /**
     * ServiceProvider constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->app = $container;
    }
}