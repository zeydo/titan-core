<?php

namespace Titan\Libraries\Mail;

use Titan\Kernel\ServiceProvider;
use PHPMailer\PHPMailer\PHPMailer;

class MailServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('mail', Mail::class);
    }
}