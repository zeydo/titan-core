<?php

namespace Titan\Libraries\Language;

use Titan\Kernel\ServiceProvider;

class LanguageServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('language', Language::class);
    }
}