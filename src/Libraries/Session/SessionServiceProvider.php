<?php

namespace Titan\Libraries\Session;

use Titan\Kernel\ServiceProvider;

class SessionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('session', Session::class);
    }
}