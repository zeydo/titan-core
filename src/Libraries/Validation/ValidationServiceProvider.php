<?php

namespace Titan\Libraries\Validation;

use Titan\Kernel\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('validation', Validation::class);
    }
}