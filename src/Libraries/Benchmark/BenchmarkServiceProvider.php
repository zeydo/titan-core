<?php

namespace Titan\Libraries\Benchmark;

use Titan\Kernel\ServiceProvider;

class BenchmarkServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('benchmark', Benchmark::class);
    }
}