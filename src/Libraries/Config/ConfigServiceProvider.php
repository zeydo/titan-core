<?php

namespace Titan\Libraries\Config;

use Titan\Kernel\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('config', Config::class);
    }
}