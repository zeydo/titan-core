<?php

namespace Titan\Libraries\Database;

use Titan\Kernel\ServiceProvider;

class DBServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('db', DB::class);
    }
}