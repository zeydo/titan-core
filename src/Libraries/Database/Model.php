<?php

namespace Titan\Libraries\Database;

use Titan\Container\Container;

class Model
{
    /**
     * Model collection
     *
     * @var array
     */
    private $modelCollection = [];

    /**
     * Default namespace
     *
     * @var null
     */
    private $namespace		 = null;

    /**
     * Application container
     *
     * @var Container
     */
    private $container;

    /**
     * Model constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Return instance of model
     *
     * @param string $model
     * @return mixed
     * @throws \ReflectionException
     */
    public function run(string $model)
    {
        $model = base64_encode($model);

        if ($this->namespace !== null) {
            $namespace	= base64_encode($this->namespace);
            $key 		= $model . '.' . $namespace;
        } else {
            $key		= $model;
        }

        // Reset namespace
        $this->namespace = null;

        if (!array_key_exists($key, $this->modelCollection)) {
            $db = $this->container->resolve('load')->model($key);
            $this->modelCollection[$key] = $db;
        }

        return $this->modelCollection[$key];
    }

    /**
     * Set namespace
     *
     * @param string $namespace
     * @return $this
     */
    public function namespace(string $namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }
}