<?php

namespace Titan\Libraries\Database;

use Titan\Kernel\ServiceProvider;

class ModelServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('model', Model::class);
    }
}