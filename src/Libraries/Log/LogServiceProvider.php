<?php

namespace Titan\Libraries\Log;

use Titan\Kernel\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('log', Log::class);
    }
}