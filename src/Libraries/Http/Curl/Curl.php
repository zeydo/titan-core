<?php

namespace Titan\Libraries\Http\Curl;

use Titan\Container\Container;

class Curl
{
    // Curl Handle
    private $ch                 = null;

    /**
     * Follow redirects
     *
     * @var bool
     */
    protected $followRedirects  = true;

    /**
     * CURLOPT Options to send with request
     *
     * @var array
     */
    protected $options          = [];

    /**
     * Headers to send with request
     *
     * @var array
     */
    protected $headers          = [];

    /**
     * Referrer url
     *
     * @var string
     */
    protected $referrer         = null;

    /**
     * Use cookie
     *
     * @var bool
     */
    protected $useCookie        = false;

    /**
     * Cookie file
     *
     * @var string
     */
    protected $cookieFile       = '';

    /**
     * User agent
     *
     * @var string
     */
    protected $userAgent        = '';

    /**
     * Response body
     *
     * @var string
     */
    protected $responseBody     = '';

    /**
     * Response headers
     *
     * @var array
     */
    protected $responseHeader   = [];

    /**
     * Error message
     *
     * @var string
     */
    private $error              = '';

    /**
     * Curl constructor
     *
     * @param Container $container
     * @return void
     */
    function __construct(Container $container)
    {
        if ($this->useCookie) {
            $this->cookieFile = $container->get('root_path') . DS . 'Storage' . DS . 'curl_cookie.txt';
        }

        if (empty($this->userAgent)) {
            $this->userAgent  = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'TitanMVC/PHP '.PHP_VERSION.' (http://github.com/tkaratug/titan2)';
        }
    }

    /**
     * Get just header of the page
     *
     * @param string $url
     * @param array $params
     * @return void
     */
    public function head(string $url, array $params = [])
    {
        $this->request('HEAD', $url, $params);
    }

    /**
     * Get request
     *
     * @param string $url
     * @param array $params
     * @return void
     */
    public function get(string $url, array $params = [])
    {
        if (!empty($params)) {
            $url .= (stripos($url, '?') !== false) ? '&' : '?';
            $url .= (is_string($params)) ? $params : http_build_query($params, '', '&');
        }
        $this->request('GET', $url);
    }

    /**
     * Post request
     *
     * @param string $url
     * @param array $params
     * @return void
     */
    public function post(string $url, array $params = [])
    {
        $this->request('POST', $url, $params);
    }

    /**
     * Put request
     *
     * @param string $url
     * @param array $params
     * @return void
     */
    public function put(string $url, array $params = [])
    {
        $this->request('PUT', $url, $params);
    }

    /**
     * Delete request
     *
     * @param string $url
     * @param array $params
     * @return void
     */
    public function delete(string $url, array $params = [])
    {
        $this->request('DELETE', $url, $params);
    }

    /**
     * Return response header
     *
     * @param string $key
     * @return array|string
     */
    public function responseHeader(string $key = null)
    {
        if (is_null($key)) {
            return $this->responseHeader;
        } else {
            if (array_key_exists($key, $this->responseHeader)) {
                return $this->responseHeader[$key];
            }

            return null;
        }

    }

    /**
     * Return response body
     *
     * @return string
     */
    public function responseBody()
    {
        return $this->responseBody;
    }

    /**
     * Define user agent
     *
     * @param string $agent
     * @return string
     */
    public function setUserAgent(string $agent)
    {
        return $this->userAgent = $agent;
    }

    /**
     * Define referrer
     *
     * @param string $referrer
     * @return string
     */
    public function setReferrer(string $referrer)
    {
        return $this->referrer = $referrer;
    }

    /**
     * Define request header
     *
     * @param array|string $header
     * @param mixed $value
     * @return mixed
     */
    public function setHeader($header, $value = null)
    {
        if (is_array($header)) {
            $this->headers = $header;
        } else {
            $this->headers[$header] = $value;
        }

        return $this->headers;
    }

    /**
     * Define request options
     *
     * @param array|string $options
     * @param mixed $value
     * @return mixed
     */
    public function setOptions($options, $value = null)
    {
        if (is_array($options)) {
            $this->options = $options;
        } else {
            $this->options[$options] = $value;
        }

        return $this->options;
    }

    /**
     * Make request
     *
     * @param string $method
     * @param string $url
     * @param array $params
     * @return void
     */
    private function request(string $method, string $url, array $params = [])
    {
        $this->error 	= '';
        $this->ch 		= curl_init();
        if (is_array($params)) $params = http_build_query($params, '', '&');

        $this->setRequestMethod($method);
        $this->setRequestOptions($url, $params);
        $this->setRequestHeaders();

        $response = curl_exec($this->ch);

        if (!empty($response)) {
            $this->getResponse($response);
        } else {
            $this->error = curl_errno($this->ch).' - '.curl_error($this->ch);
        }

        curl_close($this->ch);
    }

    /**
     * Set request method
     *
     * @param string $method
     * @return void
     */
    private function setRequestMethod(string $method)
    {
        switch (strtoupper($method)) {
            case 'HEAD':
                curl_setopt($this->ch, CURLOPT_NOBODY, true);
                break;
            case 'GET':
                curl_setopt($this->ch, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($this->ch, CURLOPT_POST, true);
                break;
            default:
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);
        }
    }

    /**
     * Set request options
     *
     * @param string $url
     * @param array $params
     * @return void
     */
    private function setRequestOptions(string $url, array $params = [])
    {
        curl_setopt($this->ch, CURLOPT_URL, $url);
        if (!empty($params)) curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);

        # Set some default CURL options
        curl_setopt($this->ch, CURLOPT_HEADER, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->userAgent);
        if ($this->useCookie !== false) {
            curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookieFile);
            curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookieFile);
        }
        if ($this->followRedirects) curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        if ($this->referrer !== null) curl_setopt($this->ch, CURLOPT_REFERER, $this->referrer);

        # Set any custom CURL options
        foreach ($this->options as $option => $value) {
            curl_setopt($this->ch, constant('CURLOPT_'.str_replace('CURLOPT_', '', strtoupper($option))), $value);
        }
    }

    /**
     * Set request headers
     *
     * @return void
     */
    private function setRequestHeaders()
    {
        $headers = [];
        foreach ($this->headers as $key => $value) {
            $headers[] = $key.': '.$value;
        }
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * Get response of the curl request
     *
     * @param string $response
     * @return void
     */
    private function getResponse(string $response)
    {
        # Headers regex
        $pattern = '#HTTP/\d\.\d.*?$.*?\r\n\r\n#ims';

        # Extract headers from response
        preg_match_all($pattern, $response, $matches);
        $headers_string 	= array_pop($matches[0]);
        $headers 			= explode("\r\n", str_replace("\r\n\r\n", '', $headers_string));

        # Remove headers from the response body
        $this->responseBody	= str_replace($headers_string, '', $response);

        # Extract the version and status from the first header
        $version_and_status = array_shift($headers);
        preg_match('#HTTP/(\d\.\d)\s(\d\d\d)\s(.*)#', $version_and_status, $matches);
        $this->responseHeader['Http-Version'] 	= $matches[1];
        $this->responseHeader['Status-Code'] 	= $matches[2];
        $this->responseHeader['Status'] 		= $matches[2].' '.$matches[3];

        # Convert headers into an associative array
        foreach ($headers as $header) {
            preg_match('#(.*?)\:\s(.*)#', $header, $matches);
            $this->responseHeader[$matches[1]] = $matches[2];
        }
    }

    /**
     * Get Error
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
}