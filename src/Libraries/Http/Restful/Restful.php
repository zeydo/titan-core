<?php

namespace Titan\Libraries\Http\Restful;

use Titan\Libraries\Http\Curl\Curl;
use Titan\Exception\ExceptionHandler;

class Restful
{
    /**
     * Curl library
     *
     * @var Curl
     */
    private $curl;

    /**
     * Restful constructor
     *
     * @param Curl $curl
     * @return void
     */
    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }

    /**
     * Make get request
     *
     * @param string $url
     * @param array $data
     * @return void
     * @throws ExceptionHandler
     */
    public function get(string $url, array $data = [])
    {
        if (!empty($url)) {
            $this->curl->get($url, $data);
        } else {
            throw new ExceptionHandler("Parametre hatası", "URL bilgisi gerekli.");
        }
    }

    /**
     * Make post request
     *
     * @param string $url
     * @param array $data
     * @return void
     * @throws ExceptionHandler
     */
    public function post(string $url, array $data = [])
    {
        if (!empty($url)) {
            $this->curl->post($url, $data);
        } else {
            throw new ExceptionHandler("Parametre hatası", "URL bilgisi gerekli.");
        }
    }

    /**
     * Make put request
     *
     * @param string $url
     * @param array $data
     * @return void
     * @throws ExceptionHandler
     */
    public function put(string $url, array $data = [])
    {
        if (!empty($url)) {
            $this->curl->put($url, $data);
        } else {
            throw new ExceptionHandler("Parametre hatası", "URL bilgisi gerekli.");
        }
    }

    /**
     * Make delete request
     *
     * @param string $url
     * @param array $data
     * @return void
     * @throws ExceptionHandler
     */
    public function delete(string $url, array $data = [])
    {
        if (!empty($url)) {
            $this->curl->delete($url, $data);
        } else {
            throw new ExceptionHandler("Parametre hatası", "URL bilgisi gerekli.");
        }
    }

    /**
     * Define request header
     *
     * @param string $header
     * @param mixed|null $value
     * @return $this
     * @throws ExceptionHandler
     */
    public function setHeader(string $header, $value = null)
    {
        if (!empty($header)) {
            $this->curl->setHeader($header, $value);
        } else {
            throw new ExceptionHandler("Parametre hatası", "Header bilgisi gerekli.");
        }

        return $this;
    }

    /**
     * Returns response
     *
     * @return string
     */
    public function response() : string
    {
        return $this->curl->responseBody();
    }

    /**
     * Returns status code
     *
     * @return int
     */
    public function statusCode() : int
    {
        return $this->curl->responseHeader('Status-Code');
    }

}
