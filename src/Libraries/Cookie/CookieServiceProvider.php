<?php

namespace Titan\Libraries\Cookie;

use Titan\Kernel\ServiceProvider;

class CookieServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('cookie', Cookie::class);
    }
}