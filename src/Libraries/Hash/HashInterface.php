<?php

namespace Titan\Libraries\Hash;

interface HashInterface
{

    /**
     * Hash the given value
     *
     * @param string $value
     * @param array $options
     * @return string
     */
    public function make(string $value, array $options = []);

    /**
     * Check the given value against a hash
     *
     * @param string $value
     * @param string $hashedValue
     * @return bool
     */
    public function check(string $value, string $hashedValue);

    /**
     * Check if the given hash has been hashed using the given options
     *
     * @param string $hashedValue
     * @param array $options
     * @return bool
     */
    public function needsRehash(string $hashedValue, array $options = []);

}