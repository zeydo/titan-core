<?php

namespace Titan\Libraries\Hash;

use Titan\Kernel\ServiceProvider;

class HashServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('hash', Hash::class);
    }
}