<?php

namespace Titan\Libraries\View;

use Titan\Kernel\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('view', View::class);
    }
}