<?php

namespace Titan\Libraries\Load;

use Titan\Container\Container;
use Titan\Exception\ExceptionHandler;

class Load
{

    /**
     * Application container
     *
     * @var Container
     */
    private $container;

    /**
     * Load constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Load view
     *
     * @param string $file
     * @param array $data
     * @throws ExceptionHandler
     * @return mixed
     */
    public function view(string $file, array $data = [])
    {
        $filePath = $this->container->get('view_path') . DS . $file . '.php';

        if (file_exists($filePath)) {
            extract($data);
            return require $filePath;
        }

        throw new ExceptionHandler('File not found.', '<b>View : </b>' . $file);
    }

    /**
     * Load model
     *
     * @param string $key
     * @return mixed
     * @throws ExceptionHandler
     */
    public function model(string $key)
    {
        // Model directory
        $modelDir       = $this->container->get('model_path');

        if (strpos($key, '.')) {
            $model 		= explode('.', $key);
            $file		= base64_decode($model[0]);
            $namespace	= base64_decode($model[1]);

            $filePath 	= $modelDir . DS . ucfirst($namespace) . DS . ucfirst($file) . '.php';
            $class 		= 'App\\Models\\' . ucfirst($namespace) . '\\' . ucfirst($file);
        } else {
            $file		= base64_decode($key);
            $filePath 	= $modelDir . DS . ucfirst($file) . '.php';
            $class 		= 'App\\Models\\' . ucfirst($file);
        }

        if (file_exists($filePath)) {
            require_once $filePath;
            return new $class;
        }

        throw new ExceptionHandler('File not found.', '<b>Model : </b>' . ucfirst($file));
    }

    /**
     * Load config file
     *
     * @param string $file
     * @return mixed
     * @throws ExceptionHandler
     */
    public function config(string $file)
    {
        $filePath = $this->container->get('config_path') . DS . ucfirst($file) . '.php';
        if (file_exists($filePath)) {
            return require $filePath;
        }

        throw new ExceptionHandler('File not found.', '<b>Config : </b>' . ucfirst($file));
    }

    /**
     * Load file
     *
     * @param string $file
     * @return mixed
     * @throws ExceptionHandler
     */
    public function file(string $file)
    {
        if (file_exists($file)) {
            return require $file;
        }

        throw new ExceptionHandler('File not found.', '<b>File : </b>' . $file);
    }

}