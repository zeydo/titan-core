<?php

namespace Titan\Libraries\Load;

use Titan\Kernel\ServiceProvider;

class LoadServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('load', Load::class);
    }
}